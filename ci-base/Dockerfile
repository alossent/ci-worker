FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

# Install headless Java, tools to resolve dynamically assigned UID to a name (nss_wrapper and gettext) and updates
# /opt/app-root/jenkins will be the JENKINS_HOME when running as Jenkins slave.
# /opt/app-root/jenkins-launcher will receive the Jenkins slave startup script.
# Because the Kubernetes plugin for Jenkins will mount a volume in JENKINS_HOME as of 0.9, the
# scripts must be in a different folder.
RUN yum install -y java-1.8.0-openjdk-headless nss_wrapper gettext && \
    yum clean all && \
    yum update -y && \
    mkdir -p /opt/app-root/jenkins /opt/app-root/jenkins-launcher && \
    chmod -R g+w /opt/app-root/jenkins

# install useful packages for a slave
# starting from same list as Puppet-managed slaves https://gitlab.cern.ch/ai/it-puppet-hostgroup-ci/blob/qa/code/manifests/appconfig/install_packages.pp
# Should be similar to lxplus (bi hostgroup)
RUN yum install -y \
   autoconf \
   automake \
   cern-get-sso-cookie \
   cmake \
   createrepo \
   gcc \
   GitPython \
   git \
   http-parser \
   jq \
   nodejs \
   npm \
   python-ldap \
   python-setuptools \
   python-virtualenv \
   python-pip \
   PyYAML \
   rpm-build \
   vim-enhanced \
   wget \
   subversion \
   doxygen mock maven && \
   yum clean all


# Copy the entrypoint script
COPY contrib/openshift/* /opt/app-root/jenkins-launcher/

# Run the JNLP client by default
ENTRYPOINT ["/opt/app-root/jenkins-launcher/run-jnlp-client"]
